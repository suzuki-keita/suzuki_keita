package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<Branch> branches = new BranchService().getBranch();
        request.setAttribute("branches", branches);

        List<Position> positions = new PositionService().getPosition();
        request.setAttribute("positions", positions);

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();

        if (isValid(request, messages)) {
            User user = new User();
            user.setName(request.getParameter("name"));
            user.setAccount(request.getParameter("account"));
            user.setPassword(request.getParameter("password"));
            user.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
            user.setPosition_id(Integer.parseInt(request.getParameter("position_id")));
            user.setStatus("1");
            new UserService().register(user);

            response.sendRedirect("management");
        } else {
            session.setAttribute("errorMessages", messages);
            request.setAttribute("name", request.getParameter("name"));
            request.setAttribute("account", request.getParameter("account"));
            request.setAttribute("account", request.getParameter("account"));

            List<Branch> branches = new BranchService().getBranch();
            request.setAttribute("branches", branches);
            request.setAttribute("barnchID", request.getParameter("branch_id"));

            List<Position> positions = new PositionService().getPosition();
            request.setAttribute("positions", positions);

            request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
    }

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String name = request.getParameter("name");
        String account = request.getParameter("account");
        String password = request.getParameter("password");
        String branch_id = request.getParameter("branch_id");
        String position_id = request.getParameter("position_id");
        UserService userservice = new UserService();

        if(StringUtils.isBlank(name)) {
        	messages.add("名前を入力してください");
        }else if(!name.matches("^.{1,10}$")) {
        	messages.add("名前は1～10字で入力してください");
        }
        if(StringUtils.isBlank(account)) {
        	messages.add("アカウントを入力してください");
        }else if(!account.matches("^[a-z0-9]{6,20}+$")){
        	messages.add("アカウントは半角英数字6～20字で入力してください");
        }
        if(!(userservice.checkSignUpAccount(account))) {
        	messages.add("そのアカウントは既に使用されています");
        }
        if (StringUtils.isBlank(password)) {
            messages.add("パスワードを入力してください");
        }else if((password.length() < 6)||(password.length() > 20)) {
        	messages.add("パスワードは6～20字で入力してください");
        }
        if(!(request.getParameter("password").equals(request.getParameter("checkPassword")))) {
        	messages.add("パスワードと確認用パスワードの内容が異なります");
        }
        if(Integer.parseInt(branch_id) == 4) {
        	if((Integer.parseInt(position_id) == 3)||(Integer.parseInt(position_id) == 4)) {
            	messages.add("支店と役職の組み合わせが間違っています");
        	}
        }
        if(Integer.parseInt(branch_id) != 4) {
        	if((Integer.parseInt(position_id) == 1)||(Integer.parseInt(position_id) == 2)) {
            	messages.add("支店と役職の組み合わせが間違っています");
        	}
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}