<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="Content-Script-Type" content="text/javvascript">
		<script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<title>SuzukiCompany</title>
		<script type="text/javascript">
			$(function(){
				$("button").click(function(){
					$('div').show('slow');
				});
			});

		</script>
	</head>
	<body>
	<button>ボタンです</button>
		<div class="mainheader">
			<ul>
			<li><a href="./" class="tohome">SuzukiCompany</a></li>
			<li><c:out value="${loginUser.name }" />としてログイン中</li>
			</ul>
		</div>
		<div class="header">
			<ul>
			<li class="a"><a href="newpost">新規投稿</a></li>
			<c:if test="${branch_id == 4 && position_id == 1 }">
			<li class="b"><a href="management">ユーザー管理</a></li>
			</c:if>
			<li class="c"><a href="logout">ログアウト</a></li>
			</ul>
		</div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<div class="main-contents">
			<div class="narrowing">
				<h1>投稿絞込み</h1>
				<form>
					<span class="borderbottom">投稿開始日</span>
					<input type="date" name="start" value="${start}">
					<span class="borderbottom">投稿最終日</span>
					<input type="date" name="end" value="${end}">
					<span class="borderbottom">カテゴリー</span>
					<input name="category" value="${category}"><br />
					<input type="submit" class="dosearch" value="決定">
				</form>
			</div>
			<div class="sticky"><a href="./"><img src="http://design-ec.com/d/e_others_41/l_e_others_410.png" style="width: 50px;"></a></div>
			<c:forEach items="${posts}" var="post">
			<div class="postComment">
				<div class="post">
					<div class="account-name">
						<p class="name" style="text-align: right;">投稿者：<c:out value="${post.name}" /> | 投稿日：<fmt:formatDate value="${post.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></p>
						<hr style="border:0;border-top:1px dashed #333;">
					</div>
					<div class="posttitle" style="text-align: center;"><h2><c:out value="${post.title}" /></h2></div>
					<div class="category"><h3>カテゴリー：<c:out value="${post.category}" /></h3></div>
					<div class="text">
						<c:forEach var="text" items="${fn:split(post.post,'
						')}">
							<p class="turntext"><c:out value="${text}" /></p>
						</c:forEach>
					</div>
					<c:if test="${loginUser.id == post.user_id }">
						<form action="deletePost" method="post">
							<input type="hidden" name="DeletePost" value="${post.id }">
							<input type="submit" class="deletepost" onclick="return confirm('投稿を削除します。よろしいですか？')" value="削除する">
						</form><br />
					</c:if>
				</div>
				<c:forEach items="${comments}" var="comment">
					<c:if test="${post.id == comment.post_id }">
						<div class="comment">
							<p class="name" style="text-align: right;">発信者：<c:out value="${comment.name }" /> | <fmt:formatDate value="${comment.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></p>
							<hr style="border:0;border-top:1px dashed #333;">
							<c:forEach var="text" items="${fn:split(comment.comment,'
							')}">
								<p class="turntext"><c:out value="${text}" /></p>
							</c:forEach>
							<div class="commentDate"></div>
							<c:if test="${loginUser.id == comment.user_id }">
								<form action="deleteComment" method="post">
									<input type="hidden" name="DeleteComment" value="${comment.id }">
									<input type="submit" class="deletepost" onclick="return confirm('コメントを削除します。よろしいですか？')" value="削除する">
								</form>
							</c:if>
						</div>
					</c:if>
				</c:forEach>

				<div class="message">
				<span class="borderleft">この投稿にコメントする</span>
					<form action="comment" method="post">
						<input type="hidden" name="messageId" value="${post.id }">
						<textarea name="comment" class="message-box" cols="100" rows="5" maxlength="500" placeholder="コメントを入力してください"><c:out value ="${comment}" /></textarea>
						<input type="submit" class="domessage" value="コメントする">
					</form>
				</div>
			</div>
			</c:forEach>
		<div class="copyright"> Copyright(c)SuzukiCompany</div>
		</div>
	</body>
</html>