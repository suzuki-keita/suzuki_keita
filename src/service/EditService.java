package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import dao.UserDao;
import utils.CipherUtil;

public class EditService {

	//アップデート
	public void update(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            	String encPassword = CipherUtil.encrypt(user.getPassword());
                user.setPassword(encPassword);

                UserDao userDao = new UserDao();
                userDao.update(connection, user);
                commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public void updateNoPass(User user) {

        Connection connection = null;

        try {
            connection = getConnection();
            	UserDao userDao = new UserDao();
                userDao.updateNoPass(connection, user);
                commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    public List<User> getEditUser(int id) {
        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            List<User> ret = userDao.getEditUser(connection,id );

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}