package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();
        List<String> massages = new ArrayList<String>(); //メッセージ入力エラー確認用リスト

        if (isValid(request, massages)) {
	        User user = (User) session.getAttribute("loginUser");

	        Comment comment = new Comment();
	        comment.setComment(request.getParameter("comment"));
	        comment.setPost_id(Integer.parseInt(request.getParameter("messageId")));
	        comment.setUser_id(user.getId());

	        new CommentService().register(comment);
	        response.sendRedirect("./");

        }else {
        	session.setAttribute("errorMessages", massages);
        	request.setAttribute("comment",request.getParameter("comment") );
            response.sendRedirect("./");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> massages) {
    	String comment = request.getParameter("comment");
        if(comment.length() > 500) {
        	massages.add("コメントは1～500文字で入力してください");
        }else if(StringUtils.isBlank(comment)) {
        	massages.add("コメントを入力してください");
        }

        if (massages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}