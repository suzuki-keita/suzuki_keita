<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<title>ログインページ</title>
	</head>
	<body>
	<div class="mainheader">
			<ul>
			<li><a href="./" class="tohome">SuzukiCompany</a></li>
			<li>ログインページ</li>
			</ul>
		</div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>

		<div class="login-contents">
			<form action="login" method="post"><br />
				<label for="account"><span class="borderleft">アカウント名</span></label>
				<input name="account" id="account" class="input" value="${account}" maxlength="20" placeholder="アカウントを入力してください" /><br />

				<label for="password"><span class="borderleft">パスワード</span></label>
				<input name="password" type="password" id="password" class="input" maxlength="20" placeholder="パスワードを入力してください"/> <br />

				<input type="submit" class="dosearch" value="ログイン" /> <br />
			</form>
		</div>
		<div class="copyright"> Copyright(c)SuzukiCompany</div>
	</body>
</html>