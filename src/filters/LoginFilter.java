package filters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(filterName="LoginFilter", urlPatterns ="/*")
public class LoginFilter implements Filter{

	@Override
	  public void doFilter(ServletRequest request, ServletResponse response,
	      FilterChain chain) throws IOException, ServletException {

		User user = (User) ((HttpServletRequest) request).getSession().getAttribute("loginUser");
        HttpSession session = ((HttpServletRequest) request).getSession();

        String path = ((HttpServletRequest)request).getServletPath();
        if(!path.matches(".*css.*")) {
			 if(!(path.equals("/login"))) {
				 if(user == null){
			            List<String> messages = new ArrayList<String>();
			            messages.add("ログインを行ってください。");
			            session.setAttribute("errorMessages", messages);
			            ((HttpServletResponse)response).sendRedirect("login");

			            return;
			        }

			 }
		 }
	        chain.doFilter(request, response);
	}

	@Override
	  public void init(FilterConfig filterConfig) throws ServletException{
	  }

	@Override
	public void destroy(){
	  }
}


