package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Comment;
import dao.CommentDao;

public class CommentService {

	//コメントの削除
	public void deleteComment(Comment comment) {
        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            commentDao.deleteComment(connection, comment);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	//コメントの登録
	public void register(Comment comment) {
	        Connection connection = null;
	        try {
	            connection = getConnection();

	            CommentDao commentDao = new CommentDao();
	            commentDao.insert(connection, comment);

	            commit(connection);
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }

	//コメントの入手
	public List<Comment> getComment() {
        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            List<Comment> ret = commentDao.getComment(connection);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
