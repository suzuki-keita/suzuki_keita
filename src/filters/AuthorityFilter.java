package filters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(filterName="AuthorityFilter", urlPatterns = { "/management","/edit","/signup" })
public class AuthorityFilter implements Filter{

	@Override
	  public void doFilter(ServletRequest request, ServletResponse response,
	      FilterChain chain) throws IOException, ServletException {

		User user = (User) ((HttpServletRequest)request).getSession().getAttribute("loginUser");
        HttpSession session = ((HttpServletRequest) request).getSession();

        if(user != null) {
        	int branch_id = user.getBranch_id();
    		int position_id = user.getPosition_id();
			if(!((branch_id == 4)&&(position_id == 1 ))) {
				List<String> messages = new ArrayList<String>();
				messages.add("不正なパラメータが入力されました");
				session.setAttribute("errorMessages", messages);
				((HttpServletResponse)response).sendRedirect("./");
				return;
			}
        }
		chain.doFilter(request, response);
	}

	@Override
	  public void init(FilterConfig filterConfig) throws ServletException{
	  }

	@Override
	public void destroy(){
	  }
}


