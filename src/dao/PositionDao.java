package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Position;
import exception.SQLRuntimeException;


public class PositionDao {

	//支店のデータ入手
    public List<Position> getPosition(Connection connection) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM positions";

            ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<Position> positionList = toPositionList(rs);

            if (positionList.isEmpty() == true) {
                return null;
            } else {
                return positionList;
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Position> toPositionList(ResultSet rs) throws SQLException {

        List<Position> ret = new ArrayList<Position>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                Timestamp created_date = rs.getTimestamp("created_date");
                Timestamp updated_date = rs.getTimestamp("updated_date");

                Position branch = new Position();
                branch.setId(id);
                branch.setName(name);
                branch.setCreated_date(created_date);
                branch.setCreated_date(updated_date);

                ret.add(branch);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}
