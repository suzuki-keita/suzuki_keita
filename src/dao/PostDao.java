package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Post;
import exception.SQLRuntimeException;

public class PostDao {

    public void insert(Connection connection, Post post) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO posts ( ");
            sql.append("category");
            sql.append(", post");
            sql.append(", user_id");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(", title");
            sql.append(") VALUES (");
            sql.append(" ?"); // category
            sql.append(", ?"); // post
            sql.append(", ?"); // user_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(", ?"); // title
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, post.getCategory());
            ps.setString(2, post.getPost());
            ps.setInt(3, post.getUser_id());
            ps.setString(4, post.getTitle());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public List<Post> getPost(Connection connection, int num, String start, String end) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("posts.id as id, ");
            sql.append("posts.title as title , ");
            sql.append("posts.category as category , ");
            sql.append("posts.post as post , ");
            sql.append("posts.user_id as user_id, ");
            sql.append("users.account as account, ");
            sql.append("users.name as name, ");
            sql.append("posts.created_date as created_date ");
            sql.append("FROM posts ");
            sql.append("INNER JOIN users ");
            sql.append("ON posts.user_id = users.id ");
            sql.append("WHERE posts.created_date >= \""+  start  +" 00:00:00\" AND posts.created_date < \"" + end +" 23:59:59\" ");
            sql.append("ORDER BY posts.created_date DESC limit "+ num);



            ps = connection.prepareStatement(sql.toString());
            ResultSet rs = ps.executeQuery();
            List<Post> ret = toPostList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	public List<Post> getCategoryPost(Connection connection, int num, String start, String end, String category) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("posts.id as id, ");
            sql.append("posts.title as title , ");
            sql.append("posts.category as category , ");
            sql.append("posts.post as post , ");
            sql.append("posts.user_id as user_id, ");
            sql.append("users.account as account, ");
            sql.append("users.name as name, ");
            sql.append("posts.created_date as created_date ");
            sql.append("FROM posts ");
            sql.append("INNER JOIN users ");
            sql.append("ON posts.user_id = users.id ");
            sql.append("WHERE posts.created_date >= \""+  start  +"\" AND posts.created_date <= \"" + end +" 23:59:59\" ");
            sql.append("AND posts.category LIKE \"%"+ category + "%\" ");
            sql.append("ORDER BY created_date DESC limit " + num);


            ps = connection.prepareStatement(sql.toString());
            ResultSet rs = ps.executeQuery();
            List<Post> retCat = toPostList(rs);
            return retCat;
		}catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}

    private List<Post> toPostList(ResultSet rs)
            throws SQLException {

        List<Post> ret = new ArrayList<Post>();
        try {
            while (rs.next()) {
                String account = rs.getString("account");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int user_id = rs.getInt("user_id");
                String title = rs.getString("title");
                String category = rs.getString("category");
                String post = rs.getString("post");
                Timestamp created_date = rs.getTimestamp("created_date");

                Post message = new Post();
                message.setAccount(account);
                message.setName(name);
                message.setId(id);
                message.setUser_id(user_id);
                message.setTitle(title);
                message.setCategory(category);
                message.setPost(post);
                message.setCreated_date(created_date);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    //投稿削除機能
    public void deletePost(Connection connection, Post post) {

        PreparedStatement ps = null;
        try {
            String sql = " DELETE FROM posts WHERE id = ?";

            ps = connection.prepareStatement(sql);

            ps.setInt(1, post.getId());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


}