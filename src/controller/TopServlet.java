package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.Post;
import beans.User;
import service.CommentService;
import service.PostService;



@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	User user = (User) request.getSession().getAttribute("loginUser");
    	request.setAttribute("loginUser", user);

    	int branch_id = user.getBranch_id();
		int position_id = user.getPosition_id();

		request.setAttribute("branch_id", branch_id);
		request.setAttribute("position_id", position_id);

        //投稿の日付絞込み
        String start = request.getParameter("start");
        String end = request.getParameter("end");

        request.setAttribute("start", start);
		request.setAttribute("end", end);

        if(StringUtils.isBlank(start)) {
        	start = "2019-02-01";
        }
        if(StringUtils.isBlank(end)) {
            Calendar cl = Calendar.getInstance();
        	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            end = sdf.format(cl.getTime());
        }

        //投稿のカテゴリー絞込み
        String category = request.getParameter("category");
        request.setAttribute("category", category);

        if(StringUtils.isBlank(category)) {
        	List<Post> posts = new PostService().getPost(start,end);
	        request.setAttribute("posts", posts);
        }else {
        	List<Post> posts = new PostService().getCategoryPost(start,end,category);
            request.setAttribute("posts", posts);
        }

        List<Comment> comments = new CommentService().getComment();
        request.setAttribute("comments", comments);

        request.getRequestDispatcher("top.jsp").forward(request, response);
    }
}