package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import dao.UserDao;
import utils.CipherUtil;

public class UserService {

	//ユーザー一覧取得
	public List<User> getManagement() {
        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            List<User> ret = userDao.getManagement(connection);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	//アカウント重複チェック
	public boolean checkAccount(String account, int editUserId) {
		 Connection connection = null;
         UserDao userdao = new UserDao();

		 try {
			 connection = getConnection();
			 String checkAccount = userdao.checkAccount(connection, account);

	         if(checkAccount == null) {
	        	 return true;
	         }else if(checkAccount.equals(userdao.checkEditAccount(connection, editUserId))){
	        	 return true;
	         }else {
	        	 return false;
	         }

		 }catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	 }
	//アカウント重複チェック（新規登録用）
	public boolean checkSignUpAccount(String account) {
		Connection connection = null;
		UserDao userdao = new UserDao();

		 try {
			 connection = getConnection();
			 String checkAccount = userdao.checkAccount(connection, account);

		     if(checkAccount == null) {
		    	 return true;
		     }else {
		    	 return false;
		     }

		 }catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
		    rollback(connection);
		    throw e;
		} finally {
		    close(connection);
		}
	 }

	//ユーザー登録
	public void register(User user) {
	        Connection connection = null;

	        try {
	        	connection = getConnection();

	            String encPassword = CipherUtil.encrypt(user.getPassword());
	            user.setPassword(encPassword);

	            UserDao userDao = new UserDao();
	            userDao.insert(connection, user);

	            commit(connection);

	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }

	//ログインチェック
	public User login(String accountOrEmail, String password) {

	        Connection connection = null;
	        try {
	            connection = getConnection();

	            UserDao userDao = new UserDao();
	            String encPassword = CipherUtil.encrypt(password);
	            User user = userDao.getUser(connection, accountOrEmail, encPassword);

	            commit(connection);

	            return user;
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }

	 //アカウント停止
	 public void stopStatus(int id) {
		 Connection connection = null;

		 try {
	            connection = getConnection();
	            UserDao userDao = new UserDao();
	            userDao.stopStatus(connection, id);

	            commit(connection);

	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	 }

	 //アカウント復活
	 public void revivalStatus(int id) {
		 Connection connection = null;

		 try {
	            connection = getConnection();

	            UserDao userDao = new UserDao();
	            userDao.rivivalStatus(connection, id);

	            commit(connection);

	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	 }
}
