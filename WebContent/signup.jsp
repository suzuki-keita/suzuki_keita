<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規ユーザー登録ページ</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="mainheader">
			<ul>
			<li><a href="./" class="tohome">SuzukiCompany</a></li>
			<li>新規ユーザー登録ページ</li>
			</ul>
		</div>
		<div class="header">
			<ul>
			<li><a href="management">ユーザー管理</a></li>
			</ul>
		</div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<div></div>
		<div class="signup-contents">
			<form action="signup" method="post">
				<br /> <label for="name"><span class="borderleft">名前（10字以内）</span></label> <input name="name" id="name" class="input" value="${name}" maxlength="10" placeholder="名前を入力してください" /><br />
				<label for="account"><span class="borderleft">アカウント名（6～20字）</span></label> <input name="account" id="account" class="input" value="${account}" maxlength="20" placeholder="アカウントを入力してください" /> <br />
				<label for="password"><span class="borderleft">パスワード（6～20字）</span></label> <input name="password" type="password" id="password" class="input" maxlength="20" placeholder="パスワードを入力してください" /> <br />
				<label for="checkPassword"><span class="borderleft">確認用パスワード</span></label> <input name="checkPassword" type="password" id="checkPassword" class="input" maxlength="20" placeholder="確認用パスワードを入力してください" /> <br />
				<label for="branch_id"><span class="borderleft">支店名</span></label>
				<select id="branch_id" name="branch_id">
					<c:forEach items="${branches}" var="branch">
						<c:choose>
							<c:when test="${branch.id} == ${branchID} ">
								<option selected value="${branch.id }"><c:out value="${branch.name }" /></option>
							</c:when>
							<c:otherwise>
								<option value="${branch.id }"><c:out value="${branch.name}" /></option>
							</c:otherwise>
						 </c:choose>
					</c:forEach>
				</select>
				<label for="position_id"><span class="borderleft">部署・役割</span></label>
				<select id="position_id" name="position_id">
					<c:forEach items="${positions}" var="position">
						<option value="${position.id }"><c:out value="${position.name }" /></option>
					</c:forEach>
				</select><br />
				<input type="submit" class="dosearch" value="登録" />
			</form>
		</div>
		<div class="copyright">Copyright(c)SuzukiComapny</div>
	</body>
</html>