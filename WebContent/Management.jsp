<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー管理ページ</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="mainheader">
			<ul>
			<li><a href="./" class="tohome">SuzukiCompany</a></li>
			<li>ユーザー管理ページ</li>
			</ul>
		</div>
		<div class="header">
			<ul>
			<li><a href="signup">新規ユーザー登録</a></li>
			</ul>
		</div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<div class="main-contents">
			<div class="management">
				<div class="account-name">
					<table width="800px">
						<tbody >
							<tr>
								<th width="20%">アカウント名</th>
								<th width="20%">名前</th>
								<th width="15%">支店名</th>
								<th width="15%">役職</th>
								<th width="15%">アカウント状況</th>
								<th width="15%">編集</th>
							</tr>
							<c:forEach items="${managements}" var="management">
								<c:choose>
									<c:when test="${management.id == loginUser.id }">
										<tr>
											<td width="15%"><c:out value="${management.account}" /></td>
											<td ><c:out value="${management.name}" /></td>
											<td ><c:out value="${management.branch_name}" /></td>
											<td ><c:out value="${management.position_name}" /></td>
											<td ><p class="loginnow">ログイン中です</p></td>
											<td >
												<form action="edit">
													<input type="hidden" name="editid" value="${management.id }">
													<input type="submit" class="doedit" value="編集する">
												</form>
											</td>
										</tr>
									</c:when>
									<c:otherwise>
										<tr>
											<td ><c:out value="${management.account}" /></td>
											<td ><c:out value="${management.name}" /></td>
											<td ><c:out value="${management.branch_name}" /></td>
											<td ><c:out value="${management.position_name}" /></td>
											<c:if test="${management.status == 1}" >
												<td >
													<form action="status1" method="post">
														<input type="hidden" name="status" value="${management.id}">
														<input type="submit" class="active" onclick="return confirm('アカウントを停止させます。よろしいですか？')" value="活動中"></form></td>
											</c:if>
											<c:if test="${management.status == 2}" >
												<td >
													<form action="status2" method="post">
														<input type="hidden" name="status" value="${management.id}">
														<input type="submit" class="stop" onclick="return confirm('アカウントを復活させます。よろしいですか？')" value="停止中"></form></td>
											</c:if>
											<td >
												<form action="edit">
													<input type="hidden" name="editid" value="${management.id }">
													<input type="submit" class="doedit" value="編集する">
												</form>
										</tr>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<div class="copyright"> Copyright(c)SuzukiCompany</div>
		</div>
	</body>
</html>