package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Post;
import dao.PostDao;

public class PostService {

	//投稿の削除
	public void deletePost(Post post) {
	        Connection connection = null;
	        try {
	            connection = getConnection();

	            PostDao postDao = new PostDao();
	            postDao.deletePost(connection, post);

	            commit(connection);
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }

    //投稿の登録
	public void register(Post post) {

        Connection connection = null;
        try {
            connection = getConnection();

            PostDao postDao = new PostDao();
            postDao.insert(connection, post);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    private static final int LIMIT_NUM = 1000;

    //投稿の入手
    public List<Post> getPost(String start, String end) {
        Connection connection = null;
        try {
            connection = getConnection();

            PostDao postDao = new PostDao();
            List<Post> ret = postDao.getPost(connection, LIMIT_NUM, start, end);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


	//カテゴリー絞込みでの投稿入手
    public List<Post> getCategoryPost(String start, String end, String category) {
		Connection connection = null;
		try {
			connection = getConnection();

			PostDao postCategoryDao = new PostDao();
			List<Post> retCat = postCategoryDao.getCategoryPost(connection, LIMIT_NUM,  start, end, category);
			return retCat;
		}catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }

	}

}