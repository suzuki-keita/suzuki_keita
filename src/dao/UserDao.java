package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;


public class UserDao {

	//アカウント確認
	public String checkAccount(Connection connection, String account){
		PreparedStatement ps = null;
		try {
			String sql = "SELECT account FROM users WHERE account = ?";
			ps = connection.prepareStatement(sql);
            ps.setString(1, account);

			ResultSet rs = ps.executeQuery();
			String existAccount = toAccount(rs);

			return existAccount;
		}catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}
	public String checkEditAccount(Connection connection, int editUserId) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT account FROM users WHERE id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, editUserId);
			ResultSet rs = ps.executeQuery();
			String checkAccount = toAccount(rs);

			return checkAccount;
		}catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }


	}
	private String toAccount(ResultSet rs) throws SQLException {
        String ret = null;
        try {
            while (rs.next()) {
                String account = rs.getString("account");
                ret = account;
            }
            return ret;
        } finally {
            close(rs);
        }
    }


	//ユーザー一覧取得機能
	public List<User> getManagement(Connection connection) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id as id, ");
            sql.append("users.account as account, ");
            sql.append("users.name as name, ");
            sql.append("branches.name as branch_name, ");
            sql.append("positions.name as position_name, ");
            sql.append("users.status as status ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branches ");
            sql.append("ON users.branch_id = branches.id ");
            sql.append("INNER JOIN positions ");
            sql.append("ON users.position_id = positions.id ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<User> ret = toManagementList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
	private List<User> toManagementList(ResultSet rs)
            throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                String account = rs.getString("account");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                String branch_name = rs.getString("branch_name");
                String position_name = rs.getString("position_name");
                String status = rs.getString("status");

                User management = new User();
                management.setAccount(account);
                management.setName(name);
                management.setId(id);
                management.setBranch_name(branch_name);
                management.setPosition_name(position_name);
                management.setStatus(status);

                ret.add(management);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

	//ユーザー登録機能
    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {

            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("account");
            sql.append(", name");
            sql.append(", branch_id");
            sql.append(", position_id");
            sql.append(", password");
            sql.append(", status");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // account
            sql.append(", ?"); // name
            sql.append(", ?"); // branch
            sql.append(", ?"); // positon
            sql.append(", ?"); // password
            sql.append(", ?"); // status
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getName());
            ps.setInt(3, user.getBranch_id());
            ps.setInt(4, user.getPosition_id());
            ps.setString(5, user.getPassword());
            ps.setString(6, user.getStatus());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //ログイン機能
    public User getUser(Connection connection, String account, String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE account = ? AND password = ? AND status = 1";

            ps = connection.prepareStatement(sql);
            ps.setString(1, account);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String account = rs.getString("account");
                String name = rs.getString("name");
                int branch_id = rs.getInt("branch_id");
                int position_id = rs.getInt("position_id");
                String password = rs.getString("password");
                String status = rs.getString("status");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");

                User user = new User();
                user.setId(id);
                user.setAccount(account);
                user.setName(name);
                user.setBranch_id(branch_id);
                user.setPosition_id(position_id);
                user.setPassword(password);
                user.setStatus(status);
                user.setCreatedDate(createdDate);
                user.setCreatedDate(updatedDate);
                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }


    //ユーザー編集機能
    public List<User> getEditUser(Connection connection, int id) {
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id as id, ");
            sql.append("users.account as account, ");
            sql.append("users.name as name, ");
            sql.append("branches.name as branch_name, ");
            sql.append("positions.name as position_name, ");
            sql.append("users.status as status, ");
            sql.append("users.branch_id as branch_id, ");
            sql.append("users.position_id as position_id ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branches ");
            sql.append("ON users.branch_id = branches.id ");
            sql.append("INNER JOIN positions ");
            sql.append("ON users.position_id = positions.id ");
            sql.append("WHERE users.id = ? ");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<User> editUserList = toEditUsertList(rs);

            return editUserList;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
	private List<User> toEditUsertList(ResultSet rs) throws SQLException {
        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String account = rs.getString("account");
                String branch_name = rs.getString("branch_name");
                String position_name = rs.getString("position_name");
                int branch_id = rs.getInt("branch_id");
                int position_id = rs.getInt("position_id");

                User editUser = new User();
                editUser.setId(id);
                editUser.setName(name);
                editUser.setAccount(account);
                editUser.setBranch_name(branch_name);
                editUser.setPosition_name(position_name);
                editUser.setBranch_id(branch_id);
                editUser.setPosition_id(position_id);


                ret.add(editUser);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    //ユーザーアップデート機能
    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  account = ?");
            sql.append(", name = ?");
            sql.append(", branch_id = ?");
            sql.append(", position_id = ?");
            sql.append(", password = ?");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getName());
            ps.setInt(3, user.getBranch_id());
            ps.setInt(4, user.getPosition_id());
            ps.setString(5, user.getPassword());
            ps.setInt(6, user.getId());

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }

    public void updateNoPass(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  account = ?");
            sql.append(", name = ?");
            sql.append(", branch_id = ?");
            sql.append(", position_id = ?");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getName());
            ps.setInt(3, user.getBranch_id());
            ps.setInt(4, user.getPosition_id());
            ps.setInt(5, user.getId());

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }

    //アカウント停止
    public void stopStatus(Connection connection, int id){
    	 PreparedStatement ps = null;
         try {
             StringBuilder sql = new StringBuilder();
             sql.append("UPDATE users SET");
             sql.append(" status = ?");
             sql.append(", updated_date = CURRENT_TIMESTAMP");
             sql.append(" WHERE");
             sql.append(" id = ?");

             ps = connection.prepareStatement(sql.toString());

             ps.setInt(1, 2);
             ps.setInt(2, id);

             int count = ps.executeUpdate();
             if (count == 0) {
                 throw new NoRowsUpdatedRuntimeException();
             }
         } catch (SQLException e) {
             throw new SQLRuntimeException(e);
         } finally {
             close(ps);
         }
    }

    //アカウント復活
    public void rivivalStatus(Connection connection, int id){
   	 PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append(" status = ?");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, 1);
            ps.setInt(2, id);

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
   }
}