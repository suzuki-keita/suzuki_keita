<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<title>新規投稿ページ</title>
	</head>
	<body>
		<div class="mainheader">
			<ul>
			<li><a href="./" class="tohome">SuzukiCompany</a></li>
			<li>新規投稿ページ</li>
			</ul>
		</div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<div class="post-contents">
			<form action="newpost" method="post">
				<label for="title"><span class="borderleft">件名(30字以内)</span></label><br />
				<input name="title" id="title" value="${title}" maxlength="30" placeholder="件名を入力してください"/> <br />
				<label for="category"><span class="borderleft">カテゴリー名（10字以内</span>）</label><br />
				<input name="category" id="category" value="${category}" maxlength="10" placeholder="カテゴリーを入力してください"/> <br />
				<label for="post"><span class="borderleft">メッセージ内容（1000字以内）</span></label><br />
				<textarea name="post" cols="100" rows="5" class="post-box"  maxlength="1000" placeholder="メッセージを入力してください"  ><c:out value="${post}" /></textarea><br />
				<input type="submit" class="dosearch" value="投稿する"/> <br />
				<input type="hidden" name="postid" value="postid">
			</form>
		</div>
		<div class="copyright"> Copyright(c)SuzukiCompany</div>
	</body>
</html>