<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー編集ページ</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="mainheader">
			<ul>
			<li><a href="./" class="tohome">SuzukiCompany</a></li>
			<li>ユーザー編集ページ</li>
			</ul>
		</div>
		<div class="header">
			<ul>
			<li><a href="management">ユーザー管理</a></li>
			</ul>
		</div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<div class="edit-contents">
			<c:forEach items="${editUsers}" var="editUser">
				<form action="edit" method="post">
					<input type="hidden" name="editUserId" value="${editUser.id}"/>
					<label for="name"><span class="borderleft">名前（10字以内）</span></label><input name="name" value="${editUser.name}" id="name" class="input" maxlength="10" /><br />
					<label for="account"><span class="borderleft">アカウント名（6～20字）</span></label><input type="text" name="account" id="account" class="input" value="${editUser.account}" maxlength="20" /> <br />
					<label for="password"><span class="borderleft">パスワード（6～20字）</span></label><input name="password" type="password" id="password" class="input" maxlength="20" /> <br />
					<label for="checkPassword"><span class="borderleft">確認用パスワード</span></label><input name="checkPassword" type="password" id="checkPassword" class="input" maxlength="20" /> <br />
					<c:if test="${editUser.id != loginUser.id}">
						<label for="branch_id"><span class="borderleft">支店名</span></label>
						<label class="selectbox">
						<select id="branch_id" name="branch_id">
					    	<c:forEach items="${branches}" var="branch">
								<c:choose>
									<c:when test="${branch.id ==  editUser.branch_id }">
										<option selected value="${branch.id }"><c:out value="${branch.name }" /></option>
									</c:when>
									<c:otherwise>
										<option value="${branch.id }"><c:out value="${branch.name}" /></option>
									</c:otherwise>
								 </c:choose>
						    </c:forEach>
						</select></label>
						<label for="position_id"><span class="borderleft">部署・役割</span></label>
						<select id="position_id" name="position_id">
							<c:forEach items="${positions}" var="position">
							    <c:choose>
									<c:when test="${position.id ==  editUser.position_id }">
										<option selected value="${position.id }"><c:out value="${position.name }" /></option>
									</c:when>
									<c:otherwise>
										<option value="${position.id }"><c:out value="${position.name}" /></option>
									</c:otherwise>
								 </c:choose>
							</c:forEach>
						</select><br />
					</c:if>
					<input type="submit" class="dosearch" value="登録" /> <br />
				</form>
			</c:forEach>
		</div>
		<div class="copyright"> Copyright(c)SuzukiCompany</div>
    </body>
</html>