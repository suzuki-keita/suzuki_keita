package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchService;
import service.EditService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
	        HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		if(isValidURL(request, messages)) {
			List<User> editUsers = new EditService().getEditUser(Integer.parseInt(request.getParameter("editid")));
			if(!editUsers.isEmpty()) {
				request.setAttribute("editUsers", editUsers);

				User user = (User) request.getSession().getAttribute("loginUser");
				request.setAttribute("loginUser", user);

				List<Branch> branches = new BranchService().getBranch();
				request.setAttribute("branches", branches);

				List<Position> positions = new PositionService().getPosition();
				request.setAttribute("positions", positions);
				request.getRequestDispatcher("edit.jsp").forward(request, response);
			}else {
				messages.add("不正なパラメータが入力されました");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("management");
			}

		}else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");
		}
	}

	//URLバリデーションチェック
	private boolean isValidURL(HttpServletRequest request, List<String> messages) {
		String editId = request.getParameter("editid");
		if(editId == null) {
			messages.add("不正なパラメータが入力されました");
		}
		else if(!(editId.matches("^[0-9]+$"))){
			messages.add("不正なパラメータが入力されました");
		}else if(StringUtils.isBlank(editId)) {
			messages.add("不正なパラメータが入力されました");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        List<String> messages = new ArrayList<String>(); //エラー確認用messages
        HttpSession session = request.getSession();
        User editUser = getEditUser(request);

        if (isValid(request, messages)) {
            try {
            	if(StringUtils.isBlank(request.getParameter("password"))) {
                    new EditService().updateNoPass(editUser);
            	}else {
                    new EditService().update(editUser);
            	}
            } catch (NoRowsUpdatedRuntimeException e) {
                messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
                session.setAttribute("errorMessages", messages);
                request.setAttribute("editUser", editUser);
                response.sendRedirect("edit?editid="+ request.getParameter("editUserId"));
                //edit?editid=2
                return;
            }
            response.sendRedirect("management");

        } else {
            session.setAttribute("errorMessages", messages);
            request.setAttribute("editUser", editUser);
            response.sendRedirect("edit?editid="+request.getParameter("editUserId"));
        }
    }

    //ユーザー編集用データ
    private User getEditUser(HttpServletRequest request)
            throws IOException, ServletException {
    	User user = (User) request.getSession().getAttribute("loginUser");
		int userId = user.getId();
		//自分を編集するとき
    	if(Integer.parseInt(request.getParameter("editUserId")) == userId) {
    		if(StringUtils.isBlank((request.getParameter("password")))) {
        		User editUser = new User();
        		editUser.setId(Integer.parseInt(request.getParameter("editUserId")));
    			editUser.setName(request.getParameter("name"));
    			editUser.setAccount(request.getParameter("account"));
    			editUser.setBranch_id(user.getBranch_id());
    			editUser.setPosition_id(user.getPosition_id());
    			return editUser;

    		}else {
    			User editUser = new User();
    			editUser.setId(Integer.parseInt(request.getParameter("editUserId")));
    			editUser.setName(request.getParameter("name"));
    			editUser.setAccount(request.getParameter("account"));
    			editUser.setPassword(request.getParameter("password"));
    			editUser.setBranch_id(user.getBranch_id());
    			editUser.setPosition_id(user.getPosition_id());
    			return editUser;
        	}
    	}else {
    		if(StringUtils.isBlank((request.getParameter("password")))) {
        		User editUser = new User();
        		editUser.setId(Integer.parseInt(request.getParameter("editUserId")));
    			editUser.setName(request.getParameter("name"));
    			editUser.setAccount(request.getParameter("account"));
    			editUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
    			editUser.setPosition_id(Integer.parseInt(request.getParameter("position_id")));
    			return editUser;

    		}else {
    			User editUser = new User();
    			editUser.setId(Integer.parseInt(request.getParameter("editUserId")));
    			editUser.setName(request.getParameter("name"));
    			editUser.setAccount(request.getParameter("account"));
    			editUser.setPassword(request.getParameter("password"));
    			editUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
    			editUser.setPosition_id(Integer.parseInt(request.getParameter("position_id")));
    			return editUser;
        	}
    	}
    }

  //エラーチェック
    private boolean isValid(HttpServletRequest request, List<String> messages) {
    	User user = (User) request.getSession().getAttribute("loginUser");

		int editUserId = Integer.parseInt(request.getParameter("editUserId"));
		String account = request.getParameter("account");
		String branch_id = request.getParameter("branch_id");
		String position_id = request.getParameter("position_id");
		UserService userservice = new UserService();

		if(branch_id == null) {
			branch_id = String.valueOf(user.getBranch_id());
		}
		if(position_id == null) {
			position_id = String.valueOf(user.getPosition_id());
		}

        if(StringUtils.isBlank(request.getParameter("name"))) {
        	messages.add("名前を入力してください");
        }else if(!request.getParameter("name").matches("^.{1,10}$")) {
        	messages.add("名前は1～10字で入力してください");
        }
        if(StringUtils.isBlank(account)) {
        	messages.add("アカウントを入力してください");
        }else if(!account.matches("^[a-z0-9]{6,20}+$")){
        	messages.add("アカウントは半角英数字6～20字で入力してください");
        }
        if(!(userservice.checkAccount(account, editUserId))) {
        	messages.add("そのアカウントは既に使用されています");
        }
        if(!(request.getParameter("password").equals(request.getParameter("checkPassword")))) {
        	messages.add("パスワードと確認用パスワードの内容が異なります");
        }
        if(!(StringUtils.isBlank((request.getParameter("password"))))) {
            if((request.getParameter("password").length() < 6)||(request.getParameter("password").length() > 20)) {
            	messages.add("パスワードは6～20字で入力してください");
            }else if(StringUtils.isBlank(request.getParameter("password"))){
            	messages.add("パスワードは6～20字で入力してください");
            }
        }
		if(Integer.parseInt(branch_id) == 4) {
			if((Integer.parseInt(position_id) == 3)||(Integer.parseInt(position_id) == 4)) {
				messages.add("支店と役職の組み合わせが間違っています");
			}
		}
		if(Integer.parseInt(branch_id) != 4) {
        	if((Integer.parseInt(position_id) == 1)||(Integer.parseInt(position_id) == 2)) {
            	messages.add("支店と役職の組み合わせが間違っています");
        	}
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}