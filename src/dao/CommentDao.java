package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentDao {

	public void insert(Connection connection, Comment comment) {
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append("comment");
            sql.append(", post_id");
            sql.append(", user_id");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // comment
            sql.append(", ?"); // post_id
            sql.append(", ?"); // user_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, comment.getComment());
            ps.setInt(2, comment.getPost_id());
            ps.setInt(3, comment.getUser_id());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<Comment> getComment(Connection connection) {
        PreparedStatement ps = null;
        try {
        	StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.comment as comment , ");
            sql.append("comments.post_id as post_id , ");
            sql.append("comments.user_id as user_id, ");
            sql.append("users.name as name, ");
            sql.append("comments.created_date as created_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Comment> commentList = toCommentList(rs);

            return commentList;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Comment> toCommentList(ResultSet rs) throws SQLException {
        List<Comment> ret = new ArrayList<Comment>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String comment = rs.getString("comment");
                int post_id = rs.getInt("post_id");
                int user_id = rs.getInt("user_id");
                Timestamp created_date = rs.getTimestamp("created_date");

                Comment userComment = new Comment();
                userComment.setId(id);
                userComment.setName(name);
                userComment.setComment(comment);
                userComment.setPost_id(post_id);
                userComment.setUser_id(user_id);
                userComment.setCreated_date(created_date);

                ret.add(userComment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    //コメント削除機能
    public void deleteComment(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            String sql = " DELETE FROM comments WHERE id = ?";

            ps = connection.prepareStatement(sql);

            ps.setInt(1, comment.getId());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}
