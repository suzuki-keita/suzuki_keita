package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

		List<User> managements = new UserService().getManagement();
		request.setAttribute("managements", managements);

		User user = (User) request.getSession().getAttribute("loginUser");
		request.setAttribute("loginUser", user);


		request.getRequestDispatcher("management.jsp").forward(request, response);
    }
}