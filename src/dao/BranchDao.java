package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Branch;
import exception.SQLRuntimeException;


public class BranchDao {

	//支店のデータ入手
    public List<Branch> getBranch(Connection connection) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM branches ORDER BY id DESC ";

            ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<Branch> branchList = toBranchList(rs);

            if (branchList.isEmpty() == true) {
                return null;
            } else {
                return branchList;
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Branch> toBranchList(ResultSet rs) throws SQLException {

        List<Branch> ret = new ArrayList<Branch>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                Timestamp created_date = rs.getTimestamp("created_date");
                Timestamp updated_date = rs.getTimestamp("updated_date");

                Branch branch = new Branch();
                branch.setId(id);
                branch.setName(name);
                branch.setCreated_date(created_date);
                branch.setCreated_date(updated_date);

                ret.add(branch);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}
