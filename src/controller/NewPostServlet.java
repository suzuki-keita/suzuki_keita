package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Post;
import beans.User;
import service.PostService;

@WebServlet(urlPatterns = { "/newpost" })
public class NewPostServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("newpost.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> massages = new ArrayList<String>(); //メッセージ入力エラー確認用リスト

        if (isValid(request, massages) == true) {

            User user = (User) session.getAttribute("loginUser");

            Post post = new Post();
            post.setTitle(request.getParameter("title"));
            post.setCategory(request.getParameter("category"));
            post.setPost(request.getParameter("post"));
            post.setUser_id(user.getId());

            new PostService().register(post);


            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", massages);
            request.setAttribute("title",request.getParameter("title"));
            request.setAttribute("category",request.getParameter("category"));
            request.setAttribute("post",request.getParameter("post"));
            request.getRequestDispatcher("newpost.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> massages) {

        String post = request.getParameter("post");
        String title = request.getParameter("title");
        String category = request.getParameter("category");

        if(StringUtils.isBlank(title)) {
        	massages.add("件名を入力してください");
        }
        if(title.length() > 30) {
        	massages.add("件名は1～30文字で入力してください");
        }
        if(category.length() > 10) {
        	massages.add("カテゴリーは1～10文字で入力してください");
        }
        if(StringUtils.isBlank(category)) {
        	massages.add("カテゴリーを入力してください");
        }
        if (StringUtils.isBlank(post)) {
        	massages.add("メッセージを入力してください");
        }
        if (1000 < post.length()) {
        	massages.add("メッセージは1～1000文字で入力してください");
        }
        if (massages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}