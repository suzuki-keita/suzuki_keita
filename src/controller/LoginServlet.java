package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        String account = request.getParameter("account");
        String password = request.getParameter("password");
        List<String> messages = new ArrayList<String>();

        LoginService loginService = new LoginService();
        User user = loginService.login(account, password);

        HttpSession session = request.getSession();

        if(isValid(request,messages)) {
        	if (user != null) {
                session.setAttribute("loginUser", user);
                response.sendRedirect("./");

            } else{
                messages.add("アカウントまたはパスワードが誤っています");
                request.setAttribute("errorMessages", messages);
                request.setAttribute("account", request.getParameter("account"));
                request.getRequestDispatcher("login.jsp").forward(request, response);
            }
        }else {
        	request.setAttribute("errorMessages", messages);
            request.setAttribute("account", request.getParameter("account"));
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
    }
    //エラーチェック
    private boolean isValid(HttpServletRequest request, List<String> messages) {

		String account = request.getParameter("account");
		String password = request.getParameter("password");

		if((StringUtils.isBlank(account)) || (StringUtils.isBlank(password))){
			messages.add("アカウントまたはパスワードが入力されていません");
		}
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}